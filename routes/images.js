const express = require('express');
const app = express();
const url = require('url');
const fs = require('fs');
const path = require('path');

const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: 'AKIA4TYSR2CUUHH6YAGK',
  secretAccessKey: '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',
});

app.get('/images/:tipo/:img', (req, res) => {
  let tipo = req.params.tipo;
  let img = req.params.img;

  let pathImages = path.resolve(__dirname, `./../uploads/${tipo}/${img}`);

  if (fs.existsSync(pathImages)) {
    res.sendFile(pathImages);
  } else {
    let noImagePath = path.resolve(__dirname, '../assets/no-image.jpg');
    res.sendFile(noImagePath);
  }
});
app.get('/image/:img', (req, res) => {
  let img = req.params.img;

  let pathImages = path.resolve(__dirname, `../public/images/${img}`);

  /* let getURL = url.format({
    protocol: req.protocol,
    host: req.get('host'),
    pathname: 'image/',
  });
  console.log(getURL); */

  //console.log('HOLAS', req.get('host'), req.protocol, req.originalUrl);

  if (fs.existsSync(pathImages)) {
    res.sendFile(pathImages);
  } else {
    var parametros2 = {
      Bucket: 'nodejs-web',
      Key: 'public/images/' + img,
    };

    s3.getObject(parametros2, (err, data) => {
      if (err) res.status(404).end();
      //console.log(data, 'ESTO AQUI');
      fs.writeFile(
        path.resolve(__dirname, `../public/images/${img}`),
        data.Body,
        'binary',
        (err) => {
          if (err) {
            let noImagePath = path.resolve(__dirname, '../assets/no-image.jpg');
            res.sendFile(noImagePath);
          }
          res.sendFile(pathImages);
        }
      );
    });
  }
});

module.exports = app;
