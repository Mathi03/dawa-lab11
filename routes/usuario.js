const express = require('express');

const bcrypt = require('bcrypt');

const Usuario = require('../models/usuario');
const app = express();
const {verificaToken} = require('../middlewares/authentication');

app.post('/usuario', function (req, res) {
  //console.log(req.body);
  let body = req.body;

  let usuario = new Usuario({
    nombre: body.nombre,
    email: body.email,
    password: bcrypt.hashSync(body.password, 10),
    role: body.role,
  });

  usuario.save((err, usuarioBD) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }

    usuarioBD.password = null;

    res.json({
      ok: true,
      usuario: usuarioBD,
    });
  });
});

app.get('/usuario', verificaToken, async function (req, res) {
  /* Usuario.find({}).exec((err, usuarios) =>{
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      })
    }
    res.json({
        ok: true,
        usuarios,
      })
  }) */
  const Users = await Usuario.find();
  res.json(Users);
});

app.put('/usuario/:id', function (req, res) {
  let id = req.params.id;
  let body = req.body;

  Usuario.findByIdAndUpdate(id, body, {new: true}, (err, usuarioBD) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }

    res.status(200).json({
      ok: true,
      usuario: usuarioBD,
    });
  });
});

app.delete('/usuario/:id', function (req, res) {
  let id = req.params.id;

  Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }
    res.json({
      ok: true,
      usuario: usuarioBorrado,
    });
  });
});
module.exports = app;
