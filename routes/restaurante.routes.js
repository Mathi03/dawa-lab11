const express = require('express')
const Restaurant = require('../models/restaurant')
const app = express()

app.post("/restaurante", function(req, res){
  console.log(req.file);
  let body = req.body

  let publication = new Restaurant({
    name: body.name,
    description: body.description,
    imagePath: req.file.filename,
    //user_id: body.user_id 
  });
  publication.save((err, usuarioBD) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      })
    }

    usuarioBD.password = null;


    res.json({
      ok: true,
      usuario: usuarioBD,
    });
  });


  
});

app.get("/restaurante", async function(req, res){

  const Publications = await Restaurant.find();
  res.json(Publications);
})

app.put('/restaurante/:id', async (req, res) => {
  let id = req.params.id;
  const { name, description } = req.body;
  const newPublication = {name, description};
  await Restaurant.findByIdAndUpdate(id, newPublication);
  res.json({status: 'Task Updated', newPublication});
});


app.get('/restaurante_id/:id', async (req, res) => {
  const publication = await Restaurant.findById(req.params.id);
  res.json(publication);
});
module.exports = app;

/* app.delete('/publicacion/', async (req, res) => {
  await Restaurant.findByIdAndRemove(req.body.id);
  res.json({status: 'Task Deleted'});
}); */

app.delete("/restaurante/:id", function(req, res){
  let id = req.params.id;

  Restaurant.findByIdAndRemove(id, (err, usuarioBorrado) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      })
    }
    res.json({
      ok: true,
      usuario: usuarioBorrado,
    })
  })
})