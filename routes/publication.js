const express = require('express');
const Publication = require('../models/publication');
const {verificaToken} = require('../middlewares/authentication');
const {v4: uuidv4} = require('uuid');
const path = require('path');
const {nextTick} = require('process');
const url = require('url');

const fs = require('fs');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: 'AKIA4TYSR2CUUHH6YAGK',
  secretAccessKey: '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',
});

const app = express();

app.post('/publicacion', verificaToken, function (req, res) {
  let body = req.body;
  let archivo = req.files.image;
  //console.log(archivo);
  let nombreArchivo = uuidv4() + path.extname(archivo.name).toLocaleLowerCase();

  archivo.mv(`public/images/${nombreArchivo}`, (err) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err,
      });
    }
  });
  let getURL = url.format({
    protocol: req.protocol,
    host: req.get('host'),
    pathname: 'image/',
  });

  let publication = new Publication({
    title: body.title,
    subtitle: body.subtitle,
    imagePath: getURL + nombreArchivo,
    image: nombreArchivo,
    //user_id: body.user_id
  });

  publication.save((err, usuarioBD) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }

    usuarioBD.password = null;

    fs.readFile(
      path.resolve(__dirname, `../public/images/${nombreArchivo}`),
      (err, data) => {
        if (err) throw err;
        s3.putObject(
          {
            Bucket: 'nodejs-web',
            Key: 'public/images/' + nombreArchivo,
            Body: data,
          },
          (err, data) => {
            if (err) throw err;
            console.log(data);
          }
        );
      }
    );

    res.json({
      ok: true,
      usuario: usuarioBD,
    });
  });
});

app.get('/publicacion', async function (req, res) {
  const Publications = await Publication.find();
  res.json(Publications);
});
const mongoose = require('mongoose');

mongoose.set('useFindAndModify', false);
app.put('/publicacion/:id', verificaToken, async (req, res) => {
  let id = req.params.id;
  let fields = {
    title: req.body.title,
    subtitle: req.body.subtitle,
  };
  let getURL = url.format({
    protocol: req.protocol,
    host: req.get('host'),
    pathname: 'image/',
  });
  let nombreArchivo;
  if (req.files) {
    let archivo = req.files.image;
    //console.log(archivo);
    nombreArchivo = uuidv4() + path.extname(archivo.name).toLocaleLowerCase();

    archivo.mv(`public/images/${nombreArchivo}`, (err) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          err,
        });
      }
    });

    fields.imagePath = getURL + nombreArchivo;
    fields.image = nombreArchivo;
  }
  await Publication.findByIdAndUpdate(id, {$set: fields}, (err, data) => {
    if (req.files) {
      s3.deleteObject(
        {
          Bucket: 'nodejs-web',
          Key: 'public/images/' + data.image,
        },
        (err, data) => {
          if (err) throw err;
          console.log('Borrado con Exito');
        }
      );
      let pathImages = path.resolve(
        __dirname,
        `../public/images/${data.image}`
      );
      if (fs.existsSync(pathImages)) {
        fs.unlinkSync(pathImages);
      }
      fs.readFile(
        path.resolve(__dirname, `../public/images/${nombreArchivo}`),
        (err, data) => {
          if (err) throw err;
          s3.putObject(
            {
              Bucket: 'nodejs-web',
              Key: 'public/images/' + nombreArchivo,
              Body: data,
            },
            (err, data) => {
              if (err) throw err;
              console.log(data);
            }
          );
        }
      );
    }
    res.json({status: 'Task Updated', data});
  });
});

app.get('/publicacion_id/:id', verificaToken, async (req, res) => {
  const publication = await Publication.findById(req.params.id);
  res.json(publication);
});
module.exports = app;

/* app.delete('/publicacion/', async (req, res) => {
  await Publication.findByIdAndRemove(req.body.id);
  res.json({status: 'Task Deleted'});
}); */

app.delete('/publicacion/:id', verificaToken, function (req, res) {
  let id = req.params.id;
  Publication.findByIdAndRemove(id, (err, data) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }
    if (!data) {
      return res.status(400).json({
        message: "Doesn't exist document",
      });
    }

    let pathImages = path.resolve(__dirname, `../public/images/${data.image}`);
    if (fs.existsSync(pathImages)) {
      fs.unlinkSync(pathImages);
    }
    s3.deleteObject(
      {
        Bucket: 'nodejs-web',
        Key: 'public/images/' + data.image,
      },
      (err, data) => {
        if (err) throw err;
        console.log('Borrado con Exito');
      }
    );
    res.json({
      ok: true,
      usuario: data,
    });
  });
  /* await Publication.findById(id, (err, data) => {
    s3.deleteObject(
      {
        Bucket: 'nodejs-web',
        Key: 'public/images/' + data.image,
      },
      (err, data) => {
        if (err) throw err;
      }
    );
  }); */
});
