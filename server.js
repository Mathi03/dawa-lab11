require('./config/config');

const express = require('express');
const mongoose = require('mongoose');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const path = require('path');
const multer = require('multer');
const fileUpload = require('express-fileupload');
const {v4: uuidv4} = require('uuid');

//STATIC FILES
app.use('/public', express.static('public'));

//JSON
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//MIDDLEWARE
app.use(morgan('dev'));
/* const storage = multer.diskStorage({
  destination: 'public/images',
  filename: (req, file, cb) => {
    cb(null, uuidv4() + path.extname(file.originalname).toLocaleLowerCase());
  },
});
app.use(
  multer({
    storage,
  }).single('image')
); */
app.use(fileUpload({useTempFiles: true}));

//CONFIG ROUTES GLOBAL
app.use(require('./routes/index'));

//ROUTES
/* var user_routes = require('./routes/usuario'); 
app.use('/api', user_routes);

var publication_routes = require('./routes/publication'); 
app.use('/api', publication_routes); */

// CONNECTION DATA BASE
mongoose.connect(
  process.env.URLBD,
  {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true},
  (err, res) => {
    if (err) throw err;

    console.log('Base de datos conectada');
  }
);

//FRONTEND
app.use(express.static(path.join(__dirname, 'build')));
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

//LISTEN PORT
app.listen(process.env.PORT, () =>
  console.log(`Escuchando  puerto ${process.env.PORT}`)
);
