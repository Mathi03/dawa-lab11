const mongoose = require('mongoose')

let Schema = mongoose.Schema;

let publicationSchema = new Schema({
  name: {
    type: String,
    required: [true, "Nombre del Restaurante"]
  },
  description: {
    type: String,
    required: [false, "Descripcion del restaurante"]
  },
  imagePath: {
    type: String,
    require: [true, "Dirreccion de archivo"]
  },
  /* user_id: {
    type: String,
    require: [true, "Es necesario el id del usuario"]
  }, */

});

module.exports = mongoose.model('Restaurant', publicationSchema)