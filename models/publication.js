const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let publicationSchema = new Schema({
  title: {
    type: String,
    required: [true, 'El titulo de la publicacion es necesario'],
  },
  subtitle: {
    type: String,
    required: [false, 'Puede colocar un contexto'],
  },
  imagePath: {
    type: String,
    require: [true, 'Dirreccion de archivo'],
  },
  image: {
    type: String,
    require: [true, 'Nombre de la imagen'],
  },
  /* user_id: {
    type: String,
    require: [true, "Es necesario el id del usuario"]
  }, */
});

module.exports = mongoose.model('Publication', publicationSchema);
