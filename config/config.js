//Puerto
process.env.PORT = process.env.PORT || 4000;

//Entorno

process.env.NODE_ENV = process.env.NODE_ENV || 'desv';

//VENCIMIENTO DE TOKEN
//60 segundos * 60 minutos * 24 horas * 30 dias

process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30;

//SEED de autentificacion
process.env.SEED = process.env.SEED || 'este-es-el-seed-creado';

//Base de datos

let urlBD;

if (process.env.NODE_ENV === 'dev') {
  urlBD = 'mongodb://localhost:27017/lab08BD';
  console.log('Puerto Local');
} else {
  urlBD = 'mongodb+srv://Mathi03:soygenial@cluster0-v81ug.mongodb.net/lab08';
  console.log('Puerto Online');
}

process.env.URLBD = urlBD;
//console.log(process.env.NODE_ENV);

process.env.CLIENT_ID =
  process.env.CLIENT_ID ||
  '468430942622-t5f95nkuf80ehh6qd6ad11lb20oe84q1.apps.googleusercontent.com';
